class Shape {
  constructor(public area: number, public name: string) {}
}

class Square extends Shape {
  constructor(public sideLength: number, public name: string) {
    super(sideLength**2, name)
  }
}

const bobTheSquare = new Square(1, "Bob")

console.log(bobTheSquare)